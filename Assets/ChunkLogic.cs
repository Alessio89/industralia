using UnityEngine;
using System.Collections;

public class ChunkLogic : MonoBehaviour {

	public static int chunkWidth = 20;
	public static int chunkHeight = 20;

	public BaseBlock[,] chunkBlocks;

	ChunkLogic _top;
	ChunkLogic _bottom;
	ChunkLogic _left;
	ChunkLogic _right;

	public ChunkDrawing thisRenderer;
	public ChunkLogic TopChunk { 
		get {
		/*	if (_top == null)
			{
				_top = ChunksManager.instance.createChunk(transform.position + transform.up * chunkHeight);
				_top.BottomChunk = this;
			}*/
			return _top;
		}
		set
		{
			_top = value;
		}
	}

	public ChunkLogic BottomChunk
	{
		get {
			/*if (_bottom == null)
			{
				_bottom = ChunksManager.instance.createChunk(transform.position - transform.up * chunkHeight);
				_bottom.TopChunk = this;
			}*/
			return _bottom;
		}
		set { _bottom = value; }
	}

	public ChunkLogic LeftChunk
	{
		get {
			/*if (_left == null)
			{
				_left = ChunksManager.instance.createChunk(transform.position + transform.right * chunkWidth);
				_left.RightChunk = this;
			}*/
			return _left;
		}
		set { _left = value; }
	}
	public ChunkLogic RightChunk
	{
		get
		{
			/*if (_right == null)
			{
				_right = ChunksManager.instance.createChunk(transform.position - transform.right * chunkWidth);
				_right.LeftChunk = this;
			}*/
			return _right;
		}
		set { _right = value; }
	}

	public bool AsyncLoading = false;
	public bool DoneLoading = false;

	// Use this for initialization
	void Start () {
		displacement = ChunksManager.instance.displacement;
		thisRenderer = GetComponent<ChunkDrawing>();
		GenerateWithNewSeed ();
	}
	int displacement;

	public void GenerateWithNewSeed()
	{
		chunkBlocks = new BaseBlock[chunkWidth, chunkHeight];
		for(int x = 0; x < chunkWidth; x++)
			for(int y = 0; y < chunkHeight; y++)
				chunkBlocks[x,y] = new EmptyBlock();
		//generateSeaLevel ();
		//ChunksManager.instance.currentGenerator = ChunksManager.instance.pickGenerator (transform.position.x, transform.position.y);
		//Debug.Log ("Generator for " + transform.position.x + ", " + transform.position.y + " is " + ChunksManager.instance.currentGenerator.GetType ().ToString ());
		//ChunksManager.instance.currentGenerator.GenerateChunk (this);
		Generators.GetGenerator (transform.position.x, transform.position.y).GenerateChunk (this);
	}

	public int localX(int x) { return (int)(x - transform.position.x); }
	public int localY(int y) { return (int)(y - transform.position.y); }

	void generateSeaLevel()
	{
	}

	public void generateLiquids(int x, int y)
	{
		BaseBlock b = AddBlock (x, y, (byte)TileType.Water);
		b.data = WaterLevel.Full;
	}

	int generateMountainLevel(float x, float y)
	{
		float n = Mathf.PerlinNoise (x, y);
		if (n > 0.5f)
			return (Random.Range(0.0f, 1.0f) > 0.6f) ? (int)TileType.Stone : (int)TileType.Grass;
		else
			return (int)TileType.Empty;
	}
	int generateUnderGround(float x, float y)
	{
		float n = Mathf.PerlinNoise (x, y);
		if (n > 0.43f)
			return (int)TileType.Stone;
		else
			return (int)TileType.Empty;
	}
	
	// Update is called once per frame
	void Update () {	
		if (Vector3.Distance(ChunksManager.instance.player.transform.position, transform.position) < (4 * chunkWidth))
		{
			for(int x = 0; x < chunkWidth; x++)
				for(int y = 0; y < chunkHeight; y++)
					if (chunkBlocks[x,y] != null)
						chunkBlocks[x,y].Update();

			if (AsyncLoading && DoneLoading)
			{
				DoneLoading = false;
				AsyncLoading = false;
				GetComponent<ChunkDrawing>().UpdateMesh = true;
			}
		}
	}
	public Vector2 getArrayCoordinates(float x, float y)
	{
		Vector2 rVector = new Vector2 ();
		float gridX = Mathf.Floor(x - transform.position.x);
		float gridY = Mathf.Floor(y - transform.position.y);
		rVector.x = gridX;
		rVector.y = gridY + 1;
		return rVector;
	}

	public void CheckChunkExit(PlayerController player)
	{
		Vector2 p = getArrayCoordinates (player.transform.position.x, player.transform.position.y);
		if (!validCoords((int)p.x, (int)p.y))
		{
			if (p.x < 0)
			{
				if (LeftChunk == null)
				{
					ChunkLogic c = ChunksManager.instance.createChunk(transform.position - transform.right * chunkWidth, false); 
					if (c != null)
					{
						LeftChunk = c;
						LeftChunk.RightChunk = this;
					}
				}
				player.currentChunk = LeftChunk;
				
			}
			else if (p.x >= chunkBlocks.GetLength(0))
			{
				if (RightChunk == null)
				{
					ChunkLogic c = ChunksManager.instance.createChunk(transform.position + transform.right * chunkWidth, false);
					if (c != null)
					{
						RightChunk = c;
						RightChunk.LeftChunk = this;
					}
				}
				player.currentChunk = RightChunk;
			}
			else if (p.y < 0)
			{
				if (BottomChunk == null)
				{
					ChunkLogic c = ChunksManager.instance.createChunk(transform.position - transform.up * chunkHeight, false);
					if (c != null)
					{
						BottomChunk = c;
						BottomChunk.TopChunk = this;
					}
				}
				player.currentChunk = BottomChunk;
			}
			else if (p.y >= chunkBlocks.GetLength(1))
			{
				if (TopChunk == null)
				{
					ChunkLogic c = ChunksManager.instance.createChunk(transform.position + transform.up * chunkHeight, false);
					if (c != null)
					{
						TopChunk = c;
						TopChunk.BottomChunk = this;
					}
				}
				player.currentChunk = TopChunk;
			}
			// top e bottom
		}

	}

	public bool validCoords(int x, int y)
	{
		if (chunkBlocks == null)
			return false;
		bool success = (x >= 0 && y >= 0 && x < chunkBlocks.GetLength (0) && y < chunkBlocks.GetLength (1));
		return success;
	}

	public byte blockAt(Vector3 pos)
	{
		Vector2 p = getArrayCoordinates (pos.x, pos.y);
		if (validCoords((int)p.x, (int)p.y))
			return chunkBlocks [(int)p.x, (int)p.y].type;
		else
			return (byte)TileType.Empty;
	}

	public BaseBlock AddBlock(int x, int y, int blockType = 0)
	{
		if (validCoords(x, y))
		{
			if (chunkBlocks[x,y] != null)
			{
				chunkBlocks [x, y] = BlockRegister.Instantiate((byte)blockType);
				chunkBlocks [x, y].thisChunk = this;
				chunkBlocks [x, y].gridY = y;
				chunkBlocks [x, y].gridX = x;
				return chunkBlocks[x,y];
			}
			else
			{
				chunkBlocks[x,y] = new EmptyBlock();
				AddBlock(x,y, blockType);
			}
		}
		else
		{
			if (x < 0) // left chunk
			{
				if (LeftChunk != null)
				{
					int actualX = (chunkWidth-1) + x;
					return LeftChunk.AddBlock(actualX, y, blockType);
				}
				
			}
			else if (x >= chunkWidth) // right chunk
			{
				if (RightChunk != null)
				{
					int actualX = x - (chunkWidth - 1);
					return RightChunk.AddBlock(actualX, y, blockType);
				}
			}
			else if (y < 0) // bottom chunk
			{
				if (BottomChunk != null)
				{
					
					int actualY = (chunkHeight-1)+ y;
					return BottomChunk.AddBlock(x, actualY, blockType);
				}
			}
			else if (y >= chunkHeight) // top chunk
			{
				if (TopChunk != null)
				{
					int actualY = y - (chunkHeight-1);
					return TopChunk.AddBlock(x, actualY, blockType);
				}
			}

		}
		return null;
	}
	
	public BaseBlock RemoveBlock(int x, int y)
	{
		GetBlock (x, y).OnBreak ();
		return AddBlock (x, y, (int)TileType.Empty);
	}

	public BaseBlock GetBlock(int x, int y)
	{
		if (validCoords(x, y))
		{
			return chunkBlocks[x,y];
		}
		else
		{
			if (x < 0) // left chunk
			{
				if (LeftChunk != null)
				{
					int actualX = chunkBlocks.GetLength(0) + x;
					return LeftChunk.GetBlock(actualX, y);
				}

			}
			else if (x >= chunkWidth) // right chunk
			{
				if (RightChunk != null)
				{
					int actualX = x - chunkBlocks.GetLength(0);
					return RightChunk.GetBlock(actualX, y);
				}
			}
			else if (y < 0) // bottom chunk
			{
				if (BottomChunk != null)
				{

					int actualY = chunkBlocks.GetLength(1)+ y;
					return BottomChunk.GetBlock(x, actualY);
				}
			}
			else if (y >= chunkHeight) // top chunk
			{
				if (TopChunk != null)
				{
					int actualY = y - chunkBlocks.GetLength(0);
					return TopChunk.GetBlock(x, actualY);
				}
			}
			else
				return null;
		}
		return null;
	}

	public void OnChunkEnter()
	{
		Debug.Log ("Entered chunk " + name);
		if (TopChunk == null)
		{
			ChunkLogic c = ChunksManager.instance.createChunk(transform.position + transform.up * chunkHeight);
			if (c != null)
			{
				TopChunk = c;
				TopChunk.BottomChunk = this;
			}
		}
		if (BottomChunk == null)
		{
			ChunkLogic c = ChunksManager.instance.createChunk(transform.position - transform.up * chunkHeight);
			if (c != null)
			{
				BottomChunk = c;
				BottomChunk.TopChunk = this;
			}
		}

		if (RightChunk == null)
		{
			ChunkLogic c = ChunksManager.instance.createChunk(transform.position + transform.right * chunkWidth);
			if (c != null)
			{
				RightChunk = c;
				RightChunk.LeftChunk = this;
			}
		}
		if (LeftChunk == null)
		{
			ChunkLogic c = ChunksManager.instance.createChunk(transform.position - transform.right * chunkWidth);
			if (c != null)
			{
				LeftChunk = c;
				LeftChunk.RightChunk = this;
			}
		}
	}
}
