﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {
	public GameObject player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = player.transform.position;
		transform.Translate (0, 0, -30);
	}
}
