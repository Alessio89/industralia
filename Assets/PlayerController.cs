﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	ChunkLogic _currentChunk;
	public ChunkLogic currentChunk {
				get { return _currentChunk; }
				set {
					_currentChunk = value;
					_currentChunk.OnChunkEnter();
				}
		}
	public float speed = 8;
	public Vector2 gridPos { get { return currentChunk.getArrayCoordinates (transform.position.x, transform.position.y); } }

	public GameObject topSide;
	public GameObject bottomSide;
	public GameObject leftSide;
	public GameObject rightSide;
	public bool CollisionEnabled = true;

	Vector3 lastPos;
	//public int gridX { get { return 
	Dictionary<string, bool> canMove = new Dictionary<string, bool>();
	// Use this for initialization
	void Start () {
//		currentChunk = GameObject.Find ("Chunk").GetComponent<ChunkLogic> ();
		lastPos = transform.position;
	}
	Dictionary<string, bool> collision;
	// Update is called once per frame
	void Update () {
		//transform.Translate (0, -0.4f, 0);
		canMove ["top"] = true; canMove ["bottom"] = true; canMove ["left"] = true; canMove ["right"] = true;		
		if (CollisionEnabled)
			checkCollision ();
		if (Input.GetKey(KeyCode.A))
		{
			if (canMove["left"])
				transform.position += new Vector3(-speed, 0);
		}
		else if (Input.GetKey(KeyCode.D))
		{
			if (canMove["right"])
				transform.position += new Vector3(speed, 0);
		}
		if (Input.GetKey(KeyCode.W))
		{		
			if (canMove["top"])
				transform.position += new Vector3(0, speed);
		}
		else if (Input.GetKey(KeyCode.S))
		{
			if (canMove["bottom"])
				transform.position += new Vector3(0, -speed);
		}


		if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
		{
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(r, out hit))
			{
				if (hit.collider.name == "Background" && Input.GetKey(KeyCode.LeftShift))
				{
					int cX = (int)hit.point.x / ChunkLogic.chunkWidth;
					int cY = (int)hit.point.y / ChunkLogic.chunkHeight;
					Vector3 cPos = new Vector3(cX * ChunkLogic.chunkWidth, cY * ChunkLogic.chunkHeight, 0);
					ChunkLogic c = ChunksManager.instance.Chunks[cPos];
					Vector2 p = c.getArrayCoordinates(hit.point.x, hit.point.y);
					c.generateLiquids((int)p.x, (int)p.y);
					c.GetComponent<ChunkDrawing>().UpdateMesh = true;
				}
				else
				{
					ChunkLogic hitChunk = hit.collider.GetComponent<ChunkLogic>();
					if (hitChunk != null)
					{
						Vector2 p = hitChunk.getArrayCoordinates(hit.point.x, hit.point.y);
						hitChunk.RemoveBlock((int)p.x, (int)p.y);
						hitChunk.GetComponent<ChunkDrawing>().GenerateMesh();
						
					}
				}
			}
		}


		if (canMove["bottom"])
			transform.position -= transform.up * 0.05f;
		currentChunk.CheckChunkExit (this);
		
	}

	int getGridBlock(GameObject o)
	{
		Vector2 p = currentChunk.getArrayCoordinates (o.transform.position.x, o.transform.position.y);
		BaseBlock b = currentChunk.GetBlock ((int)p.x, (int)p.y);
		return (int)b.type;
	}

	void checkCollision ()
	{
		int topSideTile = getGridBlock (topSide);
		int bottomSideTile = getGridBlock (bottomSide);
		int rightSideTile = getGridBlock (rightSide);
		int leftSideTile = getGridBlock (leftSide);

		float magnitude = (transform.position - lastPos).magnitude;

		canMove ["top"] = (topSideTile == (int)TileType.Empty || topSideTile == (int)TileType.Water);
		canMove ["bottom"] = (bottomSideTile == (int)TileType.Empty || bottomSideTile == (int)TileType.Water);
		canMove ["right"] = (rightSideTile == (int)TileType.Empty || rightSideTile == (int)TileType.Water);
		canMove ["left"] = (leftSideTile == (int)TileType.Empty || leftSideTile == (int)TileType.Water);

		 /*if (topSideTile != 2)
			canMove["top"] = false;
		else
			canMove["top"] = true;
		if (bottomSideTile != 2)
			transform.position += transform.up * magnitude;
		if (rightSideTile != 2)
			transform.position -= transform.right * magnitude;
		if (leftSideTile != 2)
			transform.position += transform.right * magnitude;
*/
		lastPos = transform.position;
	}

	public void PlaceAtSeaLevel()
	{
		Vector3 p = transform.position;
		p.y = 40;
		transform.position = p;
	}
	public void ToggleCollision()
	{
		CollisionEnabled = !CollisionEnabled;

		UnityEngine.UI.Text t = GameObject.Find ("CollisionEnabledText").GetComponent<UnityEngine.UI.Text> ();
		t.text = "Collisioni: " + ((CollisionEnabled) ? "On" : "Off");
	}
}
