﻿using UnityEngine;
using System.Collections;

public class BlockSelection : MonoBehaviour {
	public static BlockSelection instance;
	public UnityEngine.UI.Text currentBlock;

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SetCurrentBlock(byte type)
	{
		//GameObject.FindWithTag ("Player").GetComponent<PlayerController> ().CurrentSelectedBlock = type;
		currentBlock.text = "Current Block: " + BlockRegister.Blocks[type].ToString ();
	}
}
