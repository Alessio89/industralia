﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChunksManager : MonoBehaviour {
	public Dictionary<Vector3, ChunkLogic> Chunks = new Dictionary<Vector3, ChunkLogic>();
	public Dictionary<Vector3, bool> ChunksState = new Dictionary<Vector3, bool> ();
	public static ChunksManager instance;
	public Material ChunkMaterial;
	public PlayerController player;
	System.Random rnd;
	public int displacement;
	public IGenerator currentGenerator;

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start () {
		BlockRegister.Initialize ();
		Generators.Initialize ();
		currentGenerator = pickGenerator (0, 0);
		
		delayedStart ();

	}

	
	public int Noise (int x, int y, float scale, float mag, float exp){		
		return (int) (Mathf.Pow ((Mathf.PerlinNoise(x/scale,y/scale)*mag),(exp) )); 		
	}

	public IGenerator pickGenerator(float x, float y)
	{
		// This is a placeholder function. Here somehow custom generator should attach and therefore use its implementation of
		// chunk generation.
		// The idea is that for each biome, a generator is used, so that every biome generation implementation has its own class
		// and can be edited without touching the other biome generations.
		// This should also keep the option of adding new generation (through modding and\or DLCs) open.
		// Have a nice day,
		// - Alessio.

		// The Generators singleton class will handle the generator picking, based on the position of the chunk.
		// Generators::GetGenerator(float x, float y) will cycle through the registered generators until it finds
		// one that has (x,y) inside its boundaries.
		// To register a generator, just define a BattleFox.Bounds struct (basically a rectangle class) with an x, y, w(idth) and h(eight) and
		// instanciate the desired generator. 
		// Using Generators::RegisterGenerator will register it and it will be used when the conditions are met.
	
		return Generators.GetGenerator (x, y);
	}

	void delayedStart()
	{
		rnd = new System.Random (Random.Range (1000, 9999));
		displacement = rnd.Next (0, 9999);
		ChunkLogic rootChunk = createChunk (Vector3.zero, true, 8);//currentGenerator.GenerateChunk (Vector3.zero);
		Debug.Log ("Root Created. " + rootChunk);
		rootChunk.name = "RootChunk";
		player = GameObject.FindWithTag ("Player").GetComponent<PlayerController> ();
		player.transform.position = rootChunk.transform.position + new Vector3 (ChunkLogic.chunkWidth / 2, ChunkLogic.chunkHeight / 2, -1);
		player.currentChunk = rootChunk;

	}
	// Update is called once per frame
	void Update () {
	
	}

	public void ActivateChunk(Vector3 position)
	{
		if (ChunkExists(position))
		{
			ChunksState[position] = true;
			Chunks[position].gameObject.SetActive(true);
		}
	}

	public void DisableChunk(Vector3 position)
	{
		if (ChunkExists(position))
		{
			ChunksState[position] = false;
			Chunks[position].gameObject.SetActive(false);
		}
	}

	public void RegisterChunk(ChunkLogic c)
	{
		Chunks [c.transform.position] = c;
		ChunksState [c.transform.position] = true;
	}

	public bool ChunkExists(Vector3 v)
	{
		return (Chunks.ContainsKey (v));
	}

	public ChunkLogic createChunk(Vector3 position, bool createAdjacent = true, int recursiveTime = 1)
	{
		if (ChunkExists(position))
		{
			return Chunks[position];
		}

		GameObject o = new GameObject ();
		o.AddComponent<MeshFilter> ();
		MeshRenderer mr = o.AddComponent<MeshRenderer> ();
		mr.material = Generators.GetGenerator(position.x, position.y).generatorMaterial;
		o.AddComponent<MeshCollider> ();
		o.AddComponent<ChunkDrawing> ();
		ChunkLogic c = o.GetComponent<ChunkLogic> ();
		c.name = "Chunks_" + Chunks.Count.ToString ();
		c.transform.position = position;
		Chunks [position] = c;

		if (createAdjacent)
		{
			if (recursiveTime > 0)
			{
				recursiveTime--;
				ChunkLogic top = ChunksManager.instance.createChunk (c.transform.position + transform.up * ChunkLogic.chunkHeight, true, recursiveTime);
				if (top != null)
				{
					c.TopChunk = top;
					c.TopChunk.BottomChunk = c;
					c.TopChunk.name = "Chunks_" + Chunks.Count.ToString () + "_Top";	
				}
				ChunkLogic bottom = ChunksManager.instance.createChunk (c.transform.position - transform.up * ChunkLogic.chunkHeight, true, recursiveTime);
				if (bottom != null)
				{
					c.BottomChunk = bottom;
					c.BottomChunk.TopChunk = c;
					c.BottomChunk.name = "Chunks_" + Chunks.Count.ToString () + "_Bottom";	
				}

				ChunkLogic left = ChunksManager.instance.createChunk (c.transform.position - transform.right * ChunkLogic.chunkWidth, true, recursiveTime);
				if (left != null)
				{
					c.LeftChunk = left;
					c.LeftChunk.RightChunk = c;
					c.LeftChunk.name = "Chunks_" + Chunks.Count.ToString () + "_Left";	
				}

				ChunkLogic right = ChunksManager.instance.createChunk (c.transform.position + transform.right * ChunkLogic.chunkWidth, true, recursiveTime);
				if (right != null)
				{
					c.RightChunk = right;
					c.RightChunk.LeftChunk = c;
					c.RightChunk.name = "Chunks_" + Chunks.Count.ToString () + "_Right";	
				}
			}
		}


		return c;
	}

	public void RegenerateWorld()
	{
		foreach(KeyValuePair<Vector3, ChunkLogic> pair in Chunks)
		{
			Destroy(pair.Value.gameObject);
		}
		Chunks.Clear ();
		ChunksState.Clear ();
		delayedStart ();
	}
}
