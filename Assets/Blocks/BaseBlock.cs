﻿using UnityEngine;
using System.Collections;

public abstract class BaseBlock
{
	protected PlayerController player { get { return GameObject.FindWithTag ("Player").GetComponent<PlayerController>(); } }
	public byte type;
	public byte data;

	public ChunkLogic thisChunk;
	public int gridX, gridY;

	public bool Falling = false;

	public BaseBlock leftBlock { get { return thisChunk.GetBlock (gridX - 1, gridY); } }
	public BaseBlock rightBlock { get { return thisChunk.GetBlock (gridX + 1, gridY); } }
	public BaseBlock topBlock { get { return thisChunk.GetBlock (gridX, gridY + 1); } }
	public BaseBlock bottomBlock { get { return thisChunk.GetBlock (gridX, gridY - 1); } }

	public Vector2 TextureUV = Vector2.zero;

	public BaseBlock(byte _type, byte _data)
	{
		type = _type;
		data = _data;
//		player = GameObject.FindWithTag ("Player").GetComponent<PlayerController> ();
	}

	protected float physicsTimer = 0;

	protected void ApplyPhysics()
	{
		physicsTimer += Time.deltaTime;
		if (physicsTimer > 0.3f)
		{
			Falling = false;
			physicsTimer = 0;
			if (thisChunk.validCoords(gridX, gridY-1))
			{
				BaseBlock b = thisChunk.chunkBlocks[gridX, gridY-1];
				if (b.type == (byte)TileType.Empty)
				{
					Falling = true;
					thisChunk.RemoveBlock(gridX, gridY);
					
					thisChunk.AddBlock(gridX, gridY-1, this.type);
					thisChunk.chunkBlocks[gridX, gridY-1].data = this.data;
					thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
				}
			}
			else
			{
				if (thisChunk.BottomChunk != null)
				{
					BaseBlock b = thisChunk.BottomChunk.chunkBlocks[gridX, ChunkLogic.chunkHeight-1];
					if (b.type == (byte)TileType.Empty)
					{
						Falling = true;
						thisChunk.RemoveBlock(gridX, gridY);
						thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
						thisChunk = thisChunk.BottomChunk;
						gridY = ChunkLogic.chunkHeight-1;
						thisChunk.AddBlock(gridX, gridY, this.type);
						thisChunk.chunkBlocks[gridX, gridY].data = this.data;
						thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
					}
				}
			}
		}
	}

	public virtual void OnBreak()
	{
		GameObject o = GameObject.Instantiate (Resources.Load ("BlockBreakParticles")) as GameObject;
		o.transform.position = new Vector3 (thisChunk.transform.position.x + gridX + 0.5f, thisChunk.transform.position.y + gridY, 0);
		GameObject.Destroy (o, 0.4f);
	}

	public virtual void Render()
	{
		thisChunk.thisRenderer.AddFace (gridX, gridY,
		                                               gridX + 1, gridY,
		                                               gridX + 1, gridY - 1,
		                                               gridX, gridY - 1, this.TextureUV);

	}

	public abstract void Update();

}