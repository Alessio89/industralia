﻿using UnityEngine;
using System.Collections;

public class IronBlock : BaseBlock {

	public IronBlock() 
	: base((byte)TileType.Iron, 0) { TextureUV = TextureHelper.Iron; }
	float timer = 0;
	public override void Update ()
	{
		timer += Time.deltaTime;
		if (timer > 5)
		{
			if (Vector3.Distance(thisChunk.transform.position, player.transform.position) < 2 * ChunkLogic.chunkWidth)
			{
				if (Random.Range(0.0f, 1.0f) > 0.7f)
				{
					if (thisChunk.validCoords(gridX-1, gridY))
					{

						thisChunk.chunkBlocks[gridX-1, gridY] = BlockRegister.Instantiate((byte)TileType.Iron);
						thisChunk.chunkBlocks[gridX-1, gridY].gridX = gridX-1;
						thisChunk.chunkBlocks[gridX-1, gridY].gridY = gridY;
						thisChunk.chunkBlocks[gridX-1, gridY].thisChunk = thisChunk;
						thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
					}
				}
			}
			timer = 0;
		}
	}

}
