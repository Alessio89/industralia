﻿using UnityEngine;
using System.Collections;

public class EmptyBlock : BaseBlock {
	public EmptyBlock()
	: base((byte)TileType.Empty, 0) {
		}

	public override void Update ()
	{

	}
}
