﻿using UnityEngine;
using System.Collections;

public class GrassBlock : BaseBlock {

	float timer = 0;
	float timeToWait  = 5;
	public GrassBlock()
	: base((byte)TileType.Grass, 0) { 

	}

	public override void Update ()
	{
		Vector2 lastTexture = TextureUV;
		if (topBlock == null || topBlock.type == (byte)TileType.Empty)
			TextureUV = TextureHelper.Grass;
		else
			TextureUV = TextureHelper.Dirt;

		if (lastTexture != TextureUV)
			thisChunk.GetComponent<ChunkDrawing>().UpdateMesh = true;
	}
}
