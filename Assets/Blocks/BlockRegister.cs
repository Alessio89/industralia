﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

public static class BlockRegister {
	public static Dictionary<byte, System.Type> Blocks = new Dictionary<byte, System.Type>();

	public static void RegisterNewBlock(byte type, System.Type blockType)
	{
		Blocks [type] = blockType;
	}

	public static BaseBlock Instantiate(byte type)
	{
		BaseBlock b = System.Activator.CreateInstance (Blocks [type]) as BaseBlock;
		return b;
	}

	public static void Initialize()
	{

		RegisterNewBlock ((byte)TileType.Grass, typeof(GrassBlock));
		RegisterNewBlock ((byte)TileType.Stone, typeof(StoneBlock));
		RegisterNewBlock ((byte)TileType.Iron, typeof(IronBlock));
		RegisterNewBlock ((byte)TileType.Empty, typeof(EmptyBlock));
		RegisterNewBlock ((byte)TileType.Water, typeof(Water));
	}
}
