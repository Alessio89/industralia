﻿using UnityEngine;
using System.Collections;

public enum TileType
{
	Empty = 0,
	Grass = 1,
	Dirt = 2,
	Stone = 3,
	Iron = 4,
	Water = 5
}

