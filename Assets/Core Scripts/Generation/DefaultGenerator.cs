﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

public class DefaultGenerator : IGenerator {

	ChunksManager manager { get { return ChunksManager.instance; } }
	public Material generatorMaterial { get { return manager.ChunkMaterial; } }
	public float tUnit { get { return 0.143f; } }

	public void GenerateChunk (ChunkLogic c)
	{
		GenerateTerrain (c);
	}

	void GenerateTerrain(ChunkLogic c)
	{
		for (int x = (int)c.transform.position.x; x < c.transform.position.x + ChunkLogic.chunkWidth; x++)
		{
			int stone = manager.Noise(x + manager.displacement, manager.displacement, 80, 15, 1);
			stone += manager.Noise (x + manager.displacement, manager.displacement, 50, 30, 1);
			stone += manager.Noise(x + manager.displacement, manager.displacement, 10, 10, 1);
			//stone += 10;
			
			
			
			int grass = manager.Noise (x + manager.displacement, manager.displacement, 100, 35, 1);
			grass += manager.Noise(x +manager.displacement, manager.displacement, 50, 30, 1);
			//grass += 10;
			
			
			for (int y = (int)c.transform.position.y; y < c.transform.position.y + ChunkLogic.chunkHeight; y++)
			{
				
				if (y < stone)
				{
					c.chunkBlocks[c.localX(x), c.localY(y)] = BlockRegister.Instantiate((byte)TileType.Stone);//(byte)TileType.Stone;				
					if (manager.Noise(x + manager.displacement, y + manager.displacement, 12, 16, 1)>10)
					{
						c.chunkBlocks[c.localX(x), c.localY(y)] = BlockRegister.Instantiate((byte)TileType.Iron);
					}
					if (manager.Noise(x + manager.displacement, (y+manager.displacement)*2, 16, 14, 1)>12)
						c.chunkBlocks[c.localX(x), c.localY(y)] = BlockRegister.Instantiate((byte)TileType.Empty);
					
				}
				else if (y < grass)
					c.chunkBlocks[c.localX(x), c.localY(y)] = BlockRegister.Instantiate((byte)TileType.Grass);//(byte)TileType.Grass;
				else
				{
					c.chunkBlocks[c.localX(x), c.localY(y)] = BlockRegister.Instantiate((byte)TileType.Empty);
				}
				c.chunkBlocks[c.localX(x), c.localY(y)].gridX = c.localX(x);
				c.chunkBlocks[c.localX(x), c.localY(y)].gridY = c.localY(y);
				c.chunkBlocks[c.localX(x), c.localY(y)].thisChunk = c;
			}
		}
	}

	void pourLiquids(ChunkLogic c)
	{
		if (c.transform.position.y < 35 && c.transform.position.y > -20)
		{
			int y = Random.Range(0, ChunkLogic.chunkHeight);
			int x = Random.Range(0, ChunkLogic.chunkWidth);
			c.AddBlock(x, y, (byte)TileType.Water);
		}
	}

	BaseBlock[] sideEmptyCells(BaseBlock b)
	{
		BaseBlock current = b.leftBlock;
		if (current == null)
			return null;
		List<BaseBlock> rValue = new List<BaseBlock> ();
		int count = 0;
		while(current.type == (byte)TileType.Empty)
		{
			rValue.Add(current);
			count++;
			current = current.leftBlock;
			if (count > 3)
				break;
		}
		current = b.rightBlock;
		while(current.type == (byte)TileType.Empty)
		{
			rValue.Add(current);
			count++;
			current = current.rightBlock;
			if (count > 6)
				break;
		}
		return rValue.ToArray();
	}
}
